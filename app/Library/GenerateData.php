<?php

namespace App\Library;

use Faker;
use DB;
use App\Library\Quota;

class GenerateData
{
    private $ext = ['B','KB','MB','GB','TB', 'PB'];
    private $space = ' ';
    private $quota;

    public function __construct()
    {
        $this->quota = new Quota();
    }

    public function getData()
    {
        $data = [];
        $customers = DB::table('customers')->pluck('id');
        $months = [
            ['start' => '-6 months', 'end' => '-5 months'],
            ['start' => '-5 months', 'end' => '-4 months'],
            ['start' => '-4 months', 'end' => '-3 months'],
            ['start' => '-3 months', 'end' => '-2 months'],
            ['start' => '-2 months', 'end' => '-1 months'],
            ['start' => '-1 month', 'end' => 'now'],
        ];

        foreach($customers->all() as $userId){
            for($m = 0; $m < count($months); $m++) {
                $data[$userId][$m] = $this->generateData($userId, $months[$m]['start'], $months[$m]['end']);
            }
        }

        return $data;
    }

    private function generateData($userId, $startDate, $endDate)
    {
        $data = [];
        $countMonth = 6;
        $minRecord = 50;
        $maxRecord = 500;

        $minRecordRand = intval(round($minRecord/$countMonth));
        $maxRecordRand = intval(round($maxRecord/$countMonth));
        $recordRand = mt_rand($minRecordRand, $maxRecordRand);

        for($i = 0; $i < $recordRand; $i++) {
            $bytesOrigin = $this->generateBytes();
            $url = $this->generateUrl();
            $date = $this->generateDate($startDate, $endDate);
            $bytes = $this->quota->convertToBytes($bytesOrigin);

            $data[$i]['user_id'] = $userId;
            $data[$i]['transferred'] = $bytesOrigin;
            $data[$i]['resource'] = $url;
            $data[$i]['bytes'] = $bytes;
            $data[$i]['created_at'] = $date;
        }

        return $data;
    }

    private function generateDate($startDate = '-6 month', $endDate = 'now')
    {
        $faker = Faker\Factory::create();
        $date = $faker->dateTimeBetween($startDate, $endDate);

        return $date->format('Y-m-d H:i:s');
    }

    private function generateUrl()
    {
        $faker = Faker\Factory::create();
        return $faker->url;
    }

    private function generateBytes()
    {
        $bytesExt = $this->ext[mt_rand(0, count($this->ext)-1)];

        $bytesStart = 1;
        $bytesEnd = 999;

        if($bytesExt == 'B') $bytesStart = 100;
        else if($bytesExt == 'TB') $bytesEnd = 10;

        $bytesRand = mt_rand($bytesStart, $bytesEnd);

        return $bytesRand . $this->space . $bytesExt;
    }
}