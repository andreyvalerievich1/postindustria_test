<?php

namespace App\Library;

class Quota
{
    private $usedQuota = [];
    private $ext = ['B','KB','MB','GB','TB', 'PB'];
    private $space = ' ';

    public function getUsedQuota($allData)
    {
        $this->usedQuota = [];

        foreach($allData as $rec){
            $this->sumQuota($rec->customer->company_id, $rec->bytes);
        }

        foreach($this->usedQuota as $key => $comp) {
            $this->usedQuota[$key] = [];
            $this->usedQuota[$key]['origin'] = $this->convertOrigin($comp);
            $this->usedQuota[$key]['bytes'] = $comp;
        }

        return $this->usedQuota;
    }

    private function sumQuota($compId, $transf)
    {
        if(!isset($this->usedQuota[$compId])) $this->usedQuota[$compId] = $transf;
        else $this->usedQuota[$compId] += $transf;
    }

    public function convertToBytes($data){
        $tmp = $this->splitQuota($data);
        $data = [$tmp[2] => $tmp[1]];

        return $this->convertBytes($data);
    }

    private function splitQuota($data)
    {
        preg_match('/(\d+)\s*(\w+)/', $data, $dataSep);
        return $dataSep;
    }

    private function convertBytes($comp)
    {
        $bytes = 0;
        $conv = 1024;

        foreach($comp as $key => $bt){
            $key = strtolower($key);
            switch($key){
                case 'b':
                    $bytes += $bt;
                    break;
                case 'kb':
                    $bytes += $bt*$conv;
                    break;
                case 'mb':
                    $bytes += $bt*$conv*$conv;
                    break;
                case 'gb':
                    $bytes += $bt*$conv*$conv*$conv;
                    break;
                case 'tb':
                    $bytes += $bt*$conv*$conv*$conv*$conv;
                    break;
            }
        }

        return $bytes;
    }

    private function convertOrigin($count, $index = 0, $size = 1024)
    {
        if($count > $size) {
            $res = $count / $size;
            $index++;
            return $this->convertOrigin($res, $index);
        }

        return round($count) . $this->space . $this->ext[$index];
    }
}