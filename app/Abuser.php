<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abuser extends Model
{
    public function customer(){
        return $this->belongsTo('App\Customer', 'user_id');
    }
}
