<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Validator;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomersRequest;
use App\Customer;

class CustomersController extends Controller
{
    public function add(CustomersRequest $request)
    {
        $data = $request->only('name', 'email', 'company_id');

        $validator = Validator::make($data, [
            'email' => [Rule::unique('customers','email')],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => $validator->errors()], 422);
        }

        $cust = new Customer();
        $name = trim($data['name']);

        $cust->name = $name;
        $cust->email = $data['email'];
        $cust->company_id = $data['company_id'];
        $cust->save();

        return response()->json(['status' => "$name added!"], 200);
    }

    public function edit(CustomersRequest $request)
    {
        $data = $request->only('id', 'name', 'email', 'company_id');

        $validator = Validator::make($data, [
            'email' => [
                Rule::unique('customers','email')->ignore($data['id']),
            ],
        ]);

        if ($validator->fails()) {
            return response()->json(['status' => $validator->errors()], 422);
        }

        $cust = Customer::find($data['id']);
        $name = trim($data['name']);

        $cust->name = $name;
        $cust->email = $data['email'];
        $cust->company_id = $data['company_id'];
        $cust->save();

        return response()->json(['status' => "$name edited!"], 200);
    }

    public function remove(Request $request)
    {
        $data = $request->only('id', 'name');

        DB::beginTransaction();

        try {
            DB::table('customers')->where('id', '=',  $data['id'])->delete();
            DB::table('abusers')->where('user_id', '=',  $data['id'])->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }

        $result = Customer::all();

        foreach($result as $key => $val) {
            $result[$key]['url'] = route('customer.edit', ['id' => $result[$key]['id']]);
            $result[$key]['companyTitle'] = $result[$key]['company']['title'];
        }

        return response()->json(['status' => "{$data['name']} removed!", 'data' => $result], 200);
    }
}
