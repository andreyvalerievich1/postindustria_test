<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompaniesRequest;
use App\Company;
use App\Customer;

class CompaniesController extends Controller
{
    public function add(CompaniesRequest $request)
    {
        $data = $request->only('title', 'quota');

        $comp = new Company;
        $title = trim($data['title']);

        $comp->title = $title;
        $comp->quota = $data['quota'];
        $comp->save();

        return response()->json(['status' => "$title added!"], 200);
    }

    public function edit(CompaniesRequest $request)
    {
        $data = $request->only('id', 'title', 'quota');

        $comp = Company::find($data['id']);

        $title = trim($data['title']);

        $comp->title = $title;
        $comp->quota = $data['quota'];
        $comp->save();

        return response()->json(['status' => "$title edited!", 'data' => $data], 200);
    }

    public function remove(Request $request)
    {
        $data = $request->only('id', 'title');

        $cust = DB::table('customers')->where('company_id', $data['id'])->pluck('id');
        $custId = $cust->all();

        DB::beginTransaction();

        try {
            DB::table('abusers')->whereIn('user_id',  $custId)->delete();
            DB::table('customers')->whereIn('id', $custId)->delete();
            DB::table('companies')->where('id', '=',  $data['id'])->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
        }

        $result = Company::all();
        $resultCust = Customer::all();

        foreach($result as $key => $val) {
            $result[$key]['url'] = route('company.edit', ['id' => $result[$key]['id']]);
        }

        foreach($resultCust as $key => $val) {
            $resultCust[$key]['url'] = route('customer.edit', ['id' => $resultCust[$key]['id']]);
            $resultCust[$key]['companyTitle'] = $resultCust[$key]['company']['title'];
        }

        return response()->json(['status' => "{$data['title']} removed!", 'data' => $result, 'dataCust' => $resultCust], 200);
    }
}
