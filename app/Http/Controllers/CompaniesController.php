<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;

class CompaniesController extends Controller
{
    public function add()
    {
        return view('companies.add');
    }

    public function edit(Request $request)
    {
        $comp = Company::find($request['id']);

        return view('companies.edit', ['id' => $comp->id, 'title' => $comp->title, 'quota' => $comp->quota]);
    }
}
