<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Company;
use App\Customer;
use App\Abuser;
use App\Library\GenerateData;
use App\Library\Quota;
//use Carbon\Carbon;

class SiteController extends Controller
{
    public function index(Request $request)
    {
        $customers = Customer::all();
        $companies = Company::all();
        $quota = new Quota();

        $blockedCompanies = [];
        $blockedCompanyId = [];
        $date = 1;
        $equl = '>=';

        if($request->get('month')) $abuser = Abuser::whereMonth('created_at', $request->get('month'))->get();
        else $abuser = Abuser::all();

        $usedQuota = $quota->getUsedQuota($abuser);

        foreach($companies as $comp){
            if(!isset($usedQuota[$comp->id]['origin'])) continue;

            $comp->usedTraffic = $usedQuota[$comp->id]['origin'];
            $comp->usedTrafficBytes = $usedQuota[$comp->id]['bytes'];

            if($quota->convertToBytes($comp->quota) < $usedQuota[$comp->id]['bytes']){
                $blockedCompanies[] = $comp;
                $blockedCompanyId[] = $comp->id;
            }
        }

        usort($blockedCompanies, function($a,$b){
            return ($b->usedTrafficBytes > $a->usedTrafficBytes);
        });

        $cust = DB::table('customers')->whereIn('company_id', $blockedCompanyId)->pluck('id');
        $custId = $cust->all();

        if($request->get('month')) {
            $date = $request->get('month');
            $equl = '=';
        }

        $transf = Abuser::whereMonth('created_at', $equl, $date)
                                ->whereIn('user_id', $custId)
                                ->orderByDesc('bytes')
                                ->limit(30)
                                ->get();

        return view('site.index', [
                                    'customers' => $customers,
                                    'companies' => $companies,
                                    'blockedCompanies' => $blockedCompanies,
                                    'transf' => $transf
                                ]);
    }

    public function generateData()
    {
        $genData = new GenerateData();
        $data = $genData->getData();

        foreach($data as $userId){
            foreach($userId as $month){
                Abuser::insert($month);
            }
        }

        return response()->json(['status' => "Data has been generated!"], 200);
    }
}
