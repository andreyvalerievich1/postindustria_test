<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Company;
use App\Customer;

class CustomersController extends Controller
{
    public function add()
    {
        $comp = Company::all();
        $res = [];

        foreach($comp as $val){
            $res[$val->id] = $val->title;
        }

        return view('customers.add', ['companies' => $res]);
    }

    public function edit(Request $request)
    {
        $cust = Customer::find($request['id']);
        $comp = Company::all();
        $companies = [];

        foreach($comp as $val){
            $companies[$val->id] = $val->title;
        }

        return view('customers.edit', [
                                        'companies' => $companies,
                                        'id' => $cust->id,
                                        'name' => $cust->name,
                                        'email' => $cust->email,
                                        'company_id' => $cust->company_id
                                    ]);
    }
}
