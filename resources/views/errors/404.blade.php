@extends('layouts.site')

@section('content')
    <div align="center">
        <div><h2>404</h2></div>

        <div>&nbsp;</div>
        <div>Page not found.</div>
        <div>&nbsp;</div>

        <div><a href="{{ route('home') }}" class="btn btn-default">Home page</a></div>
    </div>
@endsection