@extends('layouts.site')

@section('content')
    <div><h2>Edit Company</h2></div>

    <div><a href="{{ route('home') }}" class="btn btn-default">Home page</a></div>

    <div class="alert alert-success hidden" id="statusMsg"></div>

    <div class="col-md-12 col-lg-12">
        {!! Form::open(['route' => 'company.edit', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'company_edit']) !!}

        {{ Form::hidden('id', $id) }}

        <div class="form-group">
            {{ Form::label('text', 'Name (required, at least 2 characters)', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
            <div class="col-sm-12 col-md-10 col-lg-10">
                {{ Form::text('title', $title, ['class' => 'form-control', 'id' => 'ctitle', 'required' => 'true', 'minlength' => '2']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::label('quota', 'Quota (required, use [B, KB, MB, GB, TB])', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
            <div class="col-sm-12 col-md-10 col-lg-10">
                {{ Form::text('quota', $quota, ['class' => 'form-control', 'id' => 'cquota', 'required' => 'true']) }}
            </div>
        </div>

        <div class="form-group">
            {{ Form::submit('Edit', ['class' => 'submit btn btn-default', 'id' => 'company_edit_sbm']) }}
        </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('script')
    <script>
        ( function($) {
            $("#company_edit_sbm").click(function(){
                if($("#company_edit").valid()) {
                    var data = $("#company_edit").serializeArray();
                    var res = {};

                    $.each(data, function(key, val){
                        res[val.name] = val.value;
                    });

                    var request = $.ajax({
                        type: 'POST',
                        url: "{{ route('api.company.edit') }}",
                        data: JSON.stringify(res),
                        contentType: "application/json; charset=UTF-8",
                        dataType: 'json',
                    });

                    request.done(function( msg ) {
                        console.log( msg );

                        $("#statusMsg").removeClass('hidden');
                        $("#statusMsg").html(msg.status);

                        $("#ctitle").val(msg.data.title);
                        $("#cquota").val(msg.data.quota);
                    });

                    request.fail(function( jqXHR, textStatus ) {
                        console.log( "Request failed: " + textStatus );
                    });

                    return false;
                }
            });
        } )(jQuery);
    </script>
@endsection