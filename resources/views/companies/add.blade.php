@extends('layouts.site')

@section('content')
    <div><h2>Add Company</h2></div>

    <div><a href="{{ route('home') }}" class="btn btn-default">Home page</a></div>

    <div class="alert alert-success hidden" id="statusMsg"></div>

    <div class="col-md-12 col-lg-12">
        {!! Form::open(['route' => 'company.add', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'company_add']) !!}
            <div class="form-group">
                {{ Form::label('text', 'Name (required, at least 2 characters)', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
                <div class="col-sm-12 col-md-10 col-lg-10">
                    {{ Form::text('title', '', ['class' => 'form-control', 'id' => 'ctitle', 'required' => 'true', 'minlength' => '2']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('quota', 'Quota (required, use [B, KB, MB, GB, TB])', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
                <div class="col-sm-12 col-md-10 col-lg-10">
                    {{ Form::text('quota', '', ['class' => 'form-control', 'id' => 'cquota', 'required' => 'true']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::submit('Add', ['class' => 'submit btn btn-default', 'id' => 'company_add_sbm']) }}
            </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('script')
    <script>
    ( function($) {
        $("#company_add_sbm").click(function(){
            if($("#company_add").valid()) {
                var data = $("#company_add").serializeArray();
                var res = {};

                $.each(data, function(key, val){
                    res[val.name] = val.value;
                });

                var request = $.ajax({
                    type: 'POST',
                    url: "{{ route('api.company.add') }}",
                    data: JSON.stringify(res),
                    contentType: "application/json; charset=UTF-8",
                    dataType: 'json',
                });

                request.done(function( msg ) {
                    console.log( msg );

                    $("#company_add").find('input[type=text]').val('');
                    $("#statusMsg").removeClass('hidden');
                    $("#statusMsg").html(msg.status);
                });

                request.fail(function( jqXHR, textStatus ) {
                    console.log( "Request failed: " + textStatus );
                });

                return false;
            }
        });
    } )(jQuery);
    </script>
@endsection