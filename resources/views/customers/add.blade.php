@extends('layouts.site')

@section('content')
    <div><h2>Add User</h2></div>

    <div><a href="{{ route('home') }}" class="btn btn-default">Home page</a></div>

    <div class="alert alert-success hidden" id="statusMsg"></div>
    <div class="alert alert-danger hidden" id="statusError"></div>

    <div class="col-md-12 col-lg-12">
        {!! Form::open(['route' => 'customer.add', 'method' => 'post', 'class' => 'form-horizontal', 'id' => 'users_add']) !!}
            <div class="form-group">
                {{ Form::label('text', 'Name (required, at least 2 characters)', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
                <div class="col-sm-12 col-md-10 col-lg-10">
                    {{ Form::text('name', '', ['class' => 'form-control', 'id' => 'uname', 'required' => 'true', 'minlength' => '2']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('email', 'E-Mail (required)', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
                <div class="col-sm-12 col-md-10 col-lg-10">
                    {{ Form::email('email', '', ['class' => 'form-control', 'id' => 'uemail', 'required' => 'true']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::label('company', 'Company (required)', ['class' => 'col-sm-12 col-md-2 col-lg-2 control-label']) }}
                <div class="col-sm-12 col-md-10 col-lg-10">
                    {{ Form::select('company_id', $companies, null, ['class' => 'form-control company_name', 'id' => 'company', 'required']) }}
                </div>
            </div>

            <div class="form-group">
                {{ Form::submit('Add', ['class' => 'submit btn btn-default', 'id' => 'users_add_sbm']) }}
            </div>

        {!! Form::close() !!}
    </div>

@endsection

@section('script')
    <script>
    ( function($) {
        $("#users_add_sbm").click(function(){
            if($("#users_add").valid()) {
                var data = $("#users_add").serializeArray();
                var res = {};

                $.each(data, function(key, val){
                    res[val.name] = val.value;
                });

                var request = $.ajax({
                    type: 'POST',
                    url: "{{ route('api.customer.add') }}",
                    data: JSON.stringify(res),
                    contentType: "application/json; charset=UTF-8",
                    dataType: 'json',
                });

                request.done(function( msg ) {
                    console.log( msg );

                    if (!$("#statusError").hasClass('hidden')) $("#statusError").addClass('hidden');
                    $("#users_add").find('input[type=text]').val('');
                    $("#users_add").find('input[type=email]').val('');
                    $("#statusMsg").removeClass('hidden');
                    $("#statusMsg").html(msg.status);
                });

                request.fail(function( jqXHR, textStatus ) {
                    console.log( "Request failed: " + textStatus + ", jqXHR = " + jqXHR.responseText );

                    var errors = jqXHR.responseText;
                    var res = '';

                    if (!$("#statusMsg").hasClass('hidden')) $("#statusMsg").addClass('hidden');
                    $("#statusError").removeClass('hidden');
                    $("#statusError").html(errors);
                });

                return false;
            }
        });
    } )(jQuery);
    </script>
@endsection