@extends('layouts.site')

@section('content')
    <div>
        <h2>Home page</h2>
    </div>
    <div class="alert alert-success hidden" id="statusMsg"></div>
    <div id="tabs">
        <ul class="nav nav-pills col-md-3 col-lg-3">
            <li><a href="#tabs-2">Users</a></li>
            <li><a href="#tabs-3">Companies</a></li>
            <li><a href="#tabs-4">Abusers</a></li>
        </ul>
        <div class="col-md-9 col-lg-9 tab-content">
            <div id="tabs-2" class="col-md-12 col-lg-12">
                <table class="table" id="userList">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Company</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($customers as $val)
                        <tr>
                            <td>{{ $val['name'] }}</td>
                            <td>{{ $val['email'] }}</td>
                            <td>{{ $val['company']['title'] }}</td>
                            <td>
                                <a class="btn btn-default users_edit" href="{{ route('customer.edit', ['id' => $val['id']]) }}">Edit</a>
                                <a class="btn btn-danger users_delete" data-id="{{ $val['id'] }}" data-name="{{ $val['name'] }}" href="#">Remove</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <a class="btn btn-default" href="{{ route('customer.add') }}">Add</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="tabs-3" class="col-md-12 col-lg-12">
                <table class="table" id="companyList">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Quota</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($companies as $val)
                            <tr>
                                <td>{{ $val['title'] }}</td>
                                <td>{{ $val['quota'] }}</td>
                                <td>
                                    <a class="btn btn-default company_edit" href="{{ route('company.edit', ['id' => $val['id']]) }}">Edit</a>
                                    <a class="btn btn-danger company_delete" data-id="{{ $val['id'] }}" data-title="{{ $val['title'] }}" href="#">Remove</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="4">
                                <a class="btn btn-default" href="{{ route('company.add') }}">Add</a>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
            <div id="tabs-4" class="col-md-12 col-lg-12">
                <div class="form-inline">
                    <div class="list-group">
                        <div class="form-group">
                            <a class="btn btn-default reload_data" id="reloadData" href="{{ url('/') }}">Reload Data</a>
                        </div>
                        <div class="form-group">
                            <a class="btn btn-default generate_data" id="genData" href="#">Generate Data</a>
                        </div>
                    </div>
                    <div class="list-group">
                    {!! Form::open(['route' => 'home', 'method' => 'get', 'id' => 'filter_logs']) !!}
                        <div class="form-group">
                            <label>Month</label>&nbsp;
                            {!! Form::selectMonth('month',null,['class' => 'form-control month_filter']) !!}
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-default show_report" name="send_filter" value="Show report" />
                        </div>
                    {!! Form::close() !!}
                    </div>
                </div>
                <div class="view_data row">
                    <div class="col-md-12 col-lg-12">
                        <div class="header"><h3>Company logs</h3></div>
                        <table class="table table-bordered" id="companyLogs">
                            <thead>
                                <tr>
                                    <th>Company</th>
                                    <th>Used</th>
                                    <th>Quota</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($blockedCompanies as $val)
                                <tr>
                                    <td>{{ $val['title'] }}</td>
                                    <td>
                                        @if(isset($val['usedTraffic']))
                                            {{ $val['usedTraffic'] }}
                                        @endif
                                    </td>
                                    <td>{{ $val['quota'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-12 col-lg-12">
                        <div class="header"><h3>Transfer logs</h3></div>
                        <table class="table table-bordered table-hover" id="tranferLogs">
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Date/time</th>
                                    <th>Resource</th>
                                    <th>Transferred</th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($transf as $rec)
                                <tr>
                                    <td>{{ $rec->customer->name }}</td>
                                    <td>{{ date('Y M d H:i:s', strtotime($rec->created_at)) }}</td>
                                    <td>{{ $rec->resource }}</td>
                                    <td>{{ $rec->transferred }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $('#genData').click(function(){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var request = $.ajax({
                type: 'POST',
                url: "{{ route('generate.data') }}"
            });

            request.done(function (msg) {
                console.log(msg);

                $("#statusMsg").removeClass('hidden');
                $("#statusMsg").html(msg.status);

                setTimeout(function(){
                    $("#statusMsg").animate({
                        "opacity": "0"
                    },700);
                }, 2000);

                setTimeout(function(){
                    $("#statusMsg").addClass('hidden');
                    $("#statusMsg").css("opacity", "1");
                }, 2800);
            });

            request.fail(function (jqXHR, textStatus) {
                console.log("Request failed: " + textStatus);
            });

            return false;
        });

        $('body').on('click', '.users_delete', function(){
            var isDel = confirm('Are you sure you want to delete this user?');

            if(isDel) {
                var res = {id: $(this).data('id'), name: $(this).data('name')};

                var request = $.ajax({
                    type: 'POST',
                    url: "{{ route('api.customer.remove') }}",
                    data: JSON.stringify(res),
                    contentType: "application/json; charset=UTF-8",
                    dataType: 'json',
                });

                request.done(function (msg) {
                    console.log(msg);

                    $("#statusMsg").removeClass('hidden');
                    $("#statusMsg").html(msg.status);

                    var res = updateHTML(msg.data, 'users_edit', 'users_delete');

                    $("#userList tbody").html(res);
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log("Request failed: " + textStatus);
                });

                return false;
            }
            return false;
        });

        $('body').on('click', '.company_delete', function(){
            var isDel = confirm('Are you sure you want to delete this company?');

            if(isDel) {
                var res = {id: $(this).data('id'), title: $(this).data('title')};

                var request = $.ajax({
                    type: 'POST',
                    url: "{{ route('api.company.remove') }}",
                    data: JSON.stringify(res),
                    contentType: "application/json; charset=UTF-8",
                    dataType: 'json',
                });

                request.done(function (msg) {
                    console.log(msg);

                    $("#statusMsg").removeClass('hidden');
                    $("#statusMsg").html(msg.status);

                    var res = updateHTML(msg.data, 'company_edit', 'company_delete');
                    var resCust = updateHTML(msg.dataCust, 'users_edit', 'users_delete');

                    $("#companyList tbody").html(res);
                    $("#userList tbody").html(resCust);
                });

                request.fail(function (jqXHR, textStatus) {
                    console.log("Request failed: " + textStatus);
                });

                return false;
            }
            return false;
        });

        function updateHTML(data, classEdit, classDelete){
            var res = '';

            for (var i = 0; i < data.length; i++) {

                res += '<tr>';

                if(typeof data[i].title != 'undefined') {
                    res += '<td>';
                    res += data[i].title;
                    res += '</td>';
                }

                if(typeof data[i].quota != 'undefined') {
                    res += '<td>';
                    res += data[i].quota;
                    res += '</td>';
                }

                if(typeof data[i].name != 'undefined') {
                    res += '<td>';
                    res += data[i].name;
                    res += '</td>';
                }

                if(typeof data[i].email != 'undefined') {
                    res += '<td>';
                    res += data[i].email;
                    res += '</td>';
                }

                if(typeof data[i].companyTitle != 'undefined') {
                    res += '<td>';
                    res += data[i].companyTitle;
                    res += '</td>';
                }

                res += '<td>';

                res += '<a class="btn btn-default ' + classEdit + '" href="' + data[i].url + '">Edit</a>&nbsp;';
                res += '<a class="btn btn-danger ' + classDelete + '" data-id="' + data[i].id + '" data-title="' + data[i].title + '" href="#">Remove</a>';

                res += '</td>';

                res += '</tr>';
            }

            return res;
        }
    </script>
@endsection