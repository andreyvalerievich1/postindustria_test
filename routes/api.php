<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => '/customer'], function() {
    Route::post('/add', 'Api\CustomersController@add')->name('api.customer.add');
    Route::post('/edit', 'Api\CustomersController@edit')->name('api.customer.edit');
    Route::post('/remove', 'Api\CustomersController@remove')->name('api.customer.remove');
});

Route::group(['prefix' => '/company'], function() {
    Route::post('/add', 'Api\CompaniesController@add')->name('api.company.add');
    Route::post('/edit', 'Api\CompaniesController@edit')->name('api.company.edit');
    Route::post('/remove', 'Api\CompaniesController@remove')->name('api.company.remove');
});