<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SiteController@index')->name('home');

Route::group(['prefix' => '/customers'], function(){

    Route::get('/add', 'CustomersController@add')->name('customer.add');
    Route::get('/edit', 'CustomersController@edit')->name('customer.edit');

});

Route::group(['prefix' => '/companies'], function(){

    Route::get('/add', 'CompaniesController@add')->name('company.add');
    Route::get('/edit', 'CompaniesController@edit')->name('company.edit');

});

Route::post('/generate/data', 'SiteController@generateData')->name('generate.data');