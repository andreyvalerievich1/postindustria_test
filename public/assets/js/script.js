( function($) {
    $( "#tabs" ).tabs().addClass( "ui-tabs-vertical ui-helper-clearfix" );
    $( "#tabs li" ).removeClass( "ui-corner-top" ).addClass( "ui-corner-left" );

    $("#users_add").validate();

    $(".company_name").select2();
    $(".month_filter").select2();
} )(jQuery);